// dependencies
const router = require('express').Router();

// imports
const User = require('../models/Users');
const userController = require('../controllers/userControllers');
const Order = require('../models/Orders')
const authen = require('../authen');

//// ROUTES
// ADMIN get all orders
router.get('/all-orders', authen.verify, userController.getAllOrders)

module.exports = router;