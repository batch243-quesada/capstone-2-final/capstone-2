// dependencies
const router = require('express').Router();

// imports
const User = require('../models/Users');
const userController = require('../controllers/userControllers');
const cartController = require('../controllers/cartControllers');
const authen = require('../authen');

//// ROUTES
// register
router.post('/register', userController.checkUsernameExists, userController.checkEmailExists, userController.registerUser);

// login
router.post('/login', userController.loginUser);

// USER details
router.get('/account', authen.verify, userController.getUserDetails)

// retrieve Authenticated User Cart
router.get('/cart', authen.verify, userController.getUserCart);

// Authenticated User amount to pay for items ADDED TO CART
router.get('/cart/total-amount-to-pay', authen.verify, userController.cartTotal);

// retrieve Authenticated User CHECKED OUT items
router.get('/check-out', authen.verify, userController.getUserOrder);

// Authenticated User amount to pay for CHECKED OUT items
router.get('/check-out/total-amount-to-pay', authen.verify, userController.checkoutTotal);

// Authenticated User remove item from cart
router.delete('/cart/remove-from-cart', authen.verify, cartController.removeFromCart);

// update User role
router.patch('/update-role/:userId', authen.verify, userController.updateRole);

// checkout items
router.post('/cart/:productId/check-out', authen.verify, userController.checkout, userController.checkedOut);

module.exports = router;