// dependencies
const bcrypt = require('bcrypt');

// import modules
const User = require('../models/Users');
const Cart = require('../models/Cart');
const Order = require('../models/Orders')
const authen = require('../authen')

// username
const checkUsernameExists = (request, response, next) => {
	return User.find({username: request.body.username})
	.then(result => {

		if(result.length > 0) response.send(`The username, ${request.body.username}, is already taken. Please use another username.`)

		else next();
	})
}

// email
const checkEmailExists = (request, response, next) => {
	return User.find({email: request.body.email})
	.then(result => {

		if(result.length > 0) response.send(`The email, ${request.body.email}, is already taken. Please use another email.`)

		else next();
	})
}

// register
const registerUser = (request, response) => {
	let newUser = new User(
			{
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				username: request.body.username,
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				mobileNo: request.body.mobileNo
			}
		)

	return newUser
	.save()
	.then(user => {
		console.log(user);
		response.send(`Thank you, ${user.username}. You are now registered.`);
	})
	.catch(error => {
		console.log(error);
		response.send(`Sorry, there was an error during registration process. Please try again.`)
	})
}

// login
const loginUser = (request, response) => {
	return User.findOne({username: request.body.username})
	.then(result => {
		console.log(result);
		if(result === null) {
			response.send(`This username doesn't exist.`)
		} else {
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect) response.send({accessToken: authen.createAccessToken(result)});
			else response.send(`Incorrect password, please try again.`);
		}
	})
}

// modify User role
const updateRole = (request, response) => {
	const userData = authen.decode(request.headers.authorization);
	let userId = request.params.userId;

	if(userData.isAdmin) {
		return User.findById(userId)
		.then(result => {
			let update = {
				isAdmin : !result.isAdmin
			}

			return User.findByIdAndUpdate(userId, update, {new: true})
			.then(document => {
				document.password = "-------";
				console.log(document);
				response.send(document)
			})
			.catch(err => response.send(err));
		})
		.catch(err => response.send(err));
	} else response.send(`Access denied!`);
}

const getProfile = (request, response) => {
	return User.findOne({_id: request.body.id})
	.then(result => {
		if (result === null) {
			return response.send(`ObjectId is not found!`)
		} else {
			result.password = "*********";
			return response.send(result);
		}
	})
}

// get Authenticated User cart
const getUserCart = async (request, response) => {
	const userData = authen.decode(request.headers.authorization);

	if(!userData.isAdmin) {
		return Cart.find({id: userData.id})
		.then(result => {
			return Cart.aggregate([
					{$match : {
					  userId : userData.id
					}},
					{$group: {
						_id: "$productId",
						inCart: {$sum: "$quantity"},
						subtotal: {$sum: "$amount"}
					}}
				]).then(result => {
						console.log(result);
						response.send(result);
					}).catch(error => console.log(error))
		}).catch(error => console.log(error))
	} else response.send(`You do not have permission to do this action!`);	
}

// Authenticated User total amount to pay for items ADDED TO CART
const cartTotal = (request, response) => {
	const userData = authen.decode(request.headers.authorization);

	if(!userData.isAdmin) {
		return Cart.find({id: userData.id})
		.then(result => {
			return Cart.aggregate([
					{$match : {
					  userId : userData.id
					}},
					{$group: {
						_id: "$userId",
						totalAmountToPay: {$sum: "$amount"}
					}}
				]).then(result => {
						response.send(result);
					}).catch(error => console.log(error))
		}).catch(error => console.log(error))
	} else response.send(`You do not have permission to do this action!`);	
}

// Authenticated User checkout item
const checkout = async (request, response, next) => {
	const userData = authen.decode(request.headers.authorization);

	if(!userData.isAdmin) {
		let checkOutItem = await Cart.find({id: userData.id}).updateMany({productId: request.params.productId}, {
				$set: {
					status: "for shipping"
				}
			}).then(result => result).catch(error => {
				console.log(error);
				response.send(error);
			})

		next();
	} else response.send(`Attempt failed. Please use a Regular Account.`)
}

// Authenticated User checkout item continuation
const checkedOut = (request, response) => {
	const userData = authen.decode(request.headers.authorization);

	if(!userData.isAdmin) {
		return Cart.find({id: userData.id})
		.then(result => {
			return Cart.aggregate([
		    {$match : {
		      status : "for shipping"
		    }},
		    {$group : {
		      _id : "$productId",
		      quantity : {$sum : "$quantity"},
		      amount: {$sum: "$amount"}
		    }}
		  ]).then(ordered=> {
		  	console.log(ordered)
				let newOrder = new Order(
						{
							userId: userData.id,
							productId: ordered[0]._id,
							quantity: ordered[0].quantity,
							amount: ordered[0].amount
						}
					)

				console.log(newOrder);
				return newOrder
				.save()
				.then(saved => {
					console.log(saved);
					response.send(`
						CHECKED OUT

						Item: ${saved.productId}
						Quantity: ${saved.quantity} pcs
						To Pay: \u20B1${saved.amount}
						Checked out: ${saved.createdAt}
						Status: for shipping

						Thank you. We hope you enjoyed shopping on our site.`);
				})
				.catch(error => {
					console.log(error);
					response.send(`Sorry, an error has occured. Please try again.`)
				})
		  })
		})

		// next();

	} else response.send(`Attempt failed. Please use a Regular Account.`)
}

// // Remove item from cart when checked out
// const afterCheckOut = (request, response) => {
// 	const userData = authen.decode(request.headers.authorization);

// 	return Cart.find({id: userData.id})
// 	.then(result => {
// 		console.log(result);
// 		return Cart.deleteOne({productId: request.params.productId})
// 		.then(removed => {
// 			console.log(removed);
// 			response.send();
// 		}).catch(error => {
// 			console.log(error);
// 			response.send(`Sorry, an error has occured. Please try again.`);
// 		})
// 	}).catch(error => {
// 		console.log(error);
// 		response.send(`Sorry, an error has occured. Please try again.`);
// 	})
// }

// retrieve Authenticated User ordered items
const getUserOrder = (request, response) => {
	const userData = authen.decode(request.headers.authorization);

	if(!userData.isAdmin) {
		return Order.find({id: userData.id})
		.then(result => {
			return Order.aggregate([
					{$match : {
					  userId : userData.id
					}},
					{$group: {
						_id: "$productId",
						quantity: {$sum: "$quantity"},
						subtotal: {$sum: "$amount"}
					}}
				]).then(result => {
						console.log(result);
						response.send(result);
					}).catch(error => console.log(error))
		}).catch(error => console.log(error))
	} else response.send(`You do not have permission to do this action!`);	
}

// Authenticated User total amount to pay for CHECKED OUT items
const checkoutTotal = (request, response) => {
	const userData = authen.decode(request.headers.authorization);

	if(!userData.isAdmin) {
		return Order.find({id: userData.id})
		.then(result => {
			return Order.aggregate([
					{$match : {
					  userId : userData.id
					}},
					{$group: {
						_id: "$userId",
						totalAmountToPay: {$sum: "$amount"}
					}}
				]).then(result => {
						response.send(result);
					}).catch(error => console.log(error))
		}).catch(error => console.log(error))
	} else response.send(`You do not have permission to do this action!`);	
}

// retrieve ALL orders (ADMIN)
const getAllOrders= (request, response) => {
	const userData = authen.decode(request.headers.authorization);

	if(userData.isAdmin) {
		return Order.find({})
		.then(result => {
			console.log(result);
			response.send(result);
		}).catch(error => console.log(error))
	} else response.send(`You do not have permission to do this action!`);	
}


// retrieve USER details
const getUserDetails = (request, response) => {
	const userData = authen.decode(request.headers.authorization);

	return User.findById(userData.id)
	.then(result => {
		result.password = "----------";
		return response.send(result);
	}).catch (err => {
		return response.send(err);
	})
}

module.exports = {
	checkUsernameExists,
	checkEmailExists,
	registerUser,
	loginUser,
	updateRole,
	getUserCart,
	cartTotal,
	checkout,
	checkedOut,
	getUserOrder,
	checkoutTotal,
	getAllOrders,
	getUserDetails,
	// afterCheckOut
}