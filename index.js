// dependencies
require('dotenv').config()
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// routes
const userRoute = require('./routes/userRoutes');
const productRoute = require('./routes/productRoutes');
const orderRoute = require('./routes/allOrderRoutes');

// express app
const app = express();

// global middlewares
app.use(cors());
app.use(express());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// route handler
app.use('/api/user', userRoute);
app.use('/api/products', productRoute);
app.use('/api/orders', orderRoute);

// db connection
mongoose.set('strictQuery', true)

mongoose
	.connect("mongodb+srv://admin:admin@zuittbatch243.vvd8gml.mongodb.net/ECommAPI?retryWrites=true&w=majority")
	.then(() => {
		app.listen(process.env.PORT || 3001, () => {
			console.log(`Connected to database.`)
			console.log(`Server running...`)
		});
	})
	.catch(error => {
		console.log(error)
		console.log(`WARNING: Connection error.`)
	})